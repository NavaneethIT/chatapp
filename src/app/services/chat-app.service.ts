import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from "@angular/fire/auth";
import { combineLatest, Observable, of } from 'rxjs';
import { switchMap, first, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import * as firebase from 'firebase'

@Injectable({
    providedIn: 'root'
})

export class ChatAppService {
    public user$: Observable<any>;
    public isUserLogged: boolean = false;

    constructor(
        public afs: AngularFirestore,
        public afAuth: AngularFireAuth,
        public router: Router
    ) {
        this.user$ = this.afAuth.authState.pipe(switchMap(user => {
            if (user) {
                return this.afs.doc<any>(`users/${user.uid}`).valueChanges();
            } else {
                return of(null);
            }
        }));
    }

    public createUser(postData: any) {
        return this.afAuth.createUserWithEmailAndPassword(postData.emailID, postData.password);
    }

    public creatUserDB(postData: any) {
        return new Promise<any>((res, rej) => {
            this.afs.collection('users').doc(postData.uid).set(postData).then(resp => res(resp), (err) => rej(err));
        });
    }

    public getUsersList() {
        return this.afs.collection("users").snapshotChanges();
    }

    public userLogin(email: any, password: any) {
        return this.afAuth.signInWithEmailAndPassword(email, password);
    }

    public getChatDetails(chatId: any, chatId1: any) {
        return firebase.default.firestore().collection('chats').where(firebase.default.firestore.FieldPath.documentId(), "in", [chatId, chatId1]).get().then();
    }

    public getSingleChatDetails(chatID: any) {
        return this.afs.collection<any>('chats').doc(chatID).snapshotChanges().pipe(map(doc => {
            return { id: doc.payload.id, ...doc.payload.data() };
        }));
    }

    public createNewChat(chatId: any) {
        const postData = {
            chatId,
            createdAt: Date.now(),
            count: 0,
            messages: []
        };

        return new Promise<any>((res, rej) => {
            this.afs.collection('chats').doc(postData.chatId).set(postData).then(resp => res(resp), (err) => rej(err));
        });
    }

    public sendMessage(chatId: any, content: any, userName: any, uid: any) {
        const data = {
            uid,
            userName,
            content,
            createdAt: Date.now()
        };

        const ref = this.afs.collection('chats').doc(chatId);
        return ref.update({
            messages: firebase.default.firestore.FieldValue.arrayUnion(data)
        });
        
    }

    public joinUsers(chat$: Observable<any>) {
        let chat: any;
        const joinKeys: any = {};

        return chat$.pipe(
            switchMap(c => {
                chat = c;
                const uids = Array.from(new Set(c.messages.map((v: any) => v.uid)));
                const userDocs = uids.map(u =>
                    this.afs.doc(`users/${u}`).valueChanges()
                );
                return userDocs.length ? combineLatest(userDocs) : of([]);
            }),
            map(arr => {
                arr.forEach(v => (joinKeys[(<any>v).uid] = v));
                chat.messages = chat.messages.map((v: any) => {
                    return { ...v, user: joinKeys[v.uid] };
                });

                return chat;
            })
        );
    }

    async signOut() {
        await this.afAuth.signOut();
        return this.router.navigate(['/']);
    }
}
