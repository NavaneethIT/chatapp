import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ChatAppService } from '../services/chat-app.service';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    public form: FormGroup = new FormGroup({
        username: new FormControl(''),
        password: new FormControl(''),
    });
    public loginInvalid: boolean = false;
    public error: string | null | undefined;
    public currentUserInfo: any;

    constructor(
        public fb: FormBuilder,
        public chatAppService: ChatAppService,
        public router: Router,
        public getDB: AngularFirestore
    ) {
        const getCurrentUserInfo = localStorage.getItem('currentUserInfo');
        if (getCurrentUserInfo) {
            this.currentUserInfo = JSON.parse(getCurrentUserInfo);
            this.router.navigate(['chats']);
        }
    }

    ngOnInit(): void {
        this.form = this.fb.group({
            emailID: ['', Validators.email],
            password: ['', Validators.required]
        });
    }

    public login() {
        this.currentUserInfo = {};
        if (this.form.valid) {
            const emailID = this.form.get('emailID')?.value;
            const password = this.form.get('password')?.value;
            this.chatAppService.userLogin(emailID, password).then((res) => {
                firebase.default.firestore().collection('users').where('emailID', '==', emailID).get().then(userData => {
                    if (userData?.docs?.length > 0) {
                        this.chatAppService.isUserLogged = true;
                        this.currentUserInfo = userData.docs[0].data();
                        localStorage.setItem('currentUserInfo', JSON.stringify(this.currentUserInfo));
                        this.router.navigate(['/chats']);
                    }
                });
            }, (err) => {
                alert(err.message);
                console.error(err.message);
            });
        }
    }
}
