import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ChatAppService } from '../services/chat-app.service';
import * as firebase from 'firebase';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {

    public error: string | null | undefined;
    public form: FormGroup = new FormGroup({
        username: new FormControl(''),
        emailID: new FormControl(''),
        password: new FormControl(''),
    });

    constructor(
        public chatAppService: ChatAppService,
        public router: Router
    ) { }

    ngOnInit(): void {
    }

    public createNewUser() {
        if (this.form.valid) {
            this.chatAppService.createUser(this.form.value).then((res: any) => {
                const postData = {
                    username: this.form.get('username')?.value,
                    uid: res.user.uid,
                    emailID: this.form.get('emailID')?.value,
                    password: this.form.get('password')?.value
                }
                this.chatAppService.creatUserDB(postData).then((userRes) => {
                    firebase.default.firestore().collection('users').where('emailID', '==', res.user.email).get().then(userData => {
                        if (userData?.docs?.length > 0) {
                            this.chatAppService.isUserLogged = true;
                            localStorage.setItem('currentUserInfo', JSON.stringify(userData.docs[0].data()));
                            this.router.navigate(['/chats']);
                        }
                    });
                }, (err) => {
                    alert(err.message);
                });
            }, (err) => {
                alert(err.message);
            });
        }
    }

}
