import { AfterViewInit, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ChatAppService } from './services/chat-app.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit, AfterViewInit {

    public title: string = 'Chat Application';

    constructor(
        public cdRef: ChangeDetectorRef,
        public cas: ChatAppService
    ) { }

    ngOnInit() {
    }

    ngAfterViewInit() {
        this.cdRef.detectChanges();
    }
}
