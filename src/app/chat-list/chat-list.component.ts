import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ChatAppService } from '../services/chat-app.service';

@Component({
    selector: 'app-chat-list',
    templateUrl: './chat-list.component.html',
    styleUrls: ['./chat-list.component.css']
})
export class ChatListComponent implements OnInit {
    @ViewChild('chatcontent') public chatcontent: ElementRef | undefined;
    public scrolltop: number = 0;
    public getUsersList: any;
    public dupUserList: any;
    public chats$: Observable<any> = new Observable;
    public newMsg: string | undefined;
    public currentUserInfo: any;
    public currentChatUserName: any;
    public isShowChatSection: boolean = false;

    constructor(
        public cas: ChatAppService,
        private route: ActivatedRoute,
        public router: Router
    ) { }

    ngOnInit(): void {
        this.currentUserInfo = localStorage.getItem('currentUserInfo');
        this.currentUserInfo = JSON.parse(this.currentUserInfo);
        this.cas.isUserLogged = this.currentUserInfo ? true : false;
        this.getUserData();
    }

    public getUserData() {
        this.getUsersList = [];
        this.dupUserList =[];
        this.cas.getUsersList().subscribe((userRes: any) => {
            userRes = userRes.filter((ele: any) => ele.payload.doc.id !== this.currentUserInfo.uid);
            this.getUsersList = userRes;
            this.dupUserList = userRes;
        }, (err) => {
            console.error(err);
        });
    }

    public searchUser(event: any) {
        this.getUsersList = this.dupUserList.filter((val: any) => val.payload.doc.data().username.toLowerCase().includes(event?.target?.value));
    }

    public chatClick(userId: any, chatUserName: any) {
        this.getUsersList.forEach((ele: any) => {
            if (ele.payload.doc.id === userId) {
                ele.selected = true;
            } else {
                ele.selected = false;
            }
        });
        this.currentChatUserName = chatUserName;
        this.isShowChatSection = true;
        const getChatId = `${this.currentUserInfo.uid}-${userId}`;
        const getChatId1 = `${userId}-${this.currentUserInfo.uid}`;

        this.cas.getChatDetails(getChatId, getChatId1).then((chatRes: any) => {
            if (chatRes?.docs.length > 0) {
                this.getChatDetails(chatRes.docs[0].data().chatId);
            } else {
                this.getNewChat(getChatId);
            }
        }, (err) => {
            console.error(err);
        });
    }

    public getChatDetails(getChatID: any) {
        const source = this.cas.getSingleChatDetails(getChatID);
        this.chats$ = this.cas.joinUsers(source);
        setTimeout(() => this.scrolltop = this.chatcontent?.nativeElement.scrollHeight, 500);
    }

    public getNewChat(getChatId: any) {
        this.cas.createNewChat(getChatId).then((res: any) => {
            this.getChatDetails(getChatId);
            console.log(res);
        }, (err: any) => {
            console.error(err.message);
        })
    }

    public submit(chatId: string) {
        this.cas.sendMessage(chatId, this.newMsg, this.currentUserInfo.username, this.currentUserInfo.uid);
        this.newMsg = '';
        setTimeout(() => this.scrolltop = this.chatcontent?.nativeElement.scrollHeight, 500);
    }

    public trackByCreated(i: any, msg: any) {
        return msg.createdAt;
    }

    public logout() {
        localStorage.removeItem('currentUserInfo');
        this.cas.signOut();
        this.cas.isUserLogged = false;
    }
}
