// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    firebaseConfig: {
        apiKey: "AIzaSyDyKa1aNeNFxmQy2QyOvUSTs1OumgxumLM",
        authDomain: "chatapp-ddc8e.firebaseapp.com",
        databaseURL: "https://chatapp-ddc8e-default-rtdb.firebaseio.com",
        projectId: "chatapp-ddc8e",
        storageBucket: "chatapp-ddc8e.appspot.com",
        messagingSenderId: "135550851295",
        appId: "1:135550851295:web:3938074a898d10daaeb16a"
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
